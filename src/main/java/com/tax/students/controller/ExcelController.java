package com.tax.students.controller;

import com.tax.students.entity.ExcelStudent;
import com.tax.students.payload.ApiResponse;
import com.tax.students.service.ExcelService;
import com.tax.students.service.ExcelToDbImporter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/excel")
@Api(tags = "ExcelController", description = "Excel faylidagi studentlar haqidagi ma'lumotlari bilan ishlash")
public class ExcelController {
    private final ExcelService excelService;

    private ExcelController(ExcelService excelService) {
        this.excelService = excelService;
    }

    @ApiOperation(value = "Excel fayl yuklash", notes = "studentlar haqidagi ma'lumotlar to'ldirilgan excel fayl yuklanadi")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfully retrieved"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 400, message = "Please upload an excel file!"),
            @io.swagger.annotations.ApiResponse(code = 417, message = "Could not upload the file")
    })
    @PostMapping("/upload")
    public ResponseEntity<?> uploadFile(@RequestParam MultipartFile file) {
        String message = "";
        if (ExcelToDbImporter.hasExcelFormat(file)) {
            try {
                excelService.save(file);
                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(200).body(new ApiResponse(message, true));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!";
                return ResponseEntity.status(417).body(new ApiResponse(message, false));
            }
        }
        message = "Please upload an excel file!";
        return ResponseEntity.status(400).body(new ApiResponse(message, false));
    }
    @ApiOperation(value = "Excel faylidan bazaga yuklagan ma'lumotlarni olish", notes = "yuklangan hamma ma'lumotlar qaytariladi")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfully retrieved"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 400, message = "Please upload an excel file!"),
            @io.swagger.annotations.ApiResponse(code = 417, message = "Could not upload the file")
    })
    @GetMapping("/students")
    public ResponseEntity<List<ExcelStudent>> getAllTutorials() {
        try {
            List<ExcelStudent> studentList = excelService.getAllExcelStudents();
            if (studentList.isEmpty()) {
                return ResponseEntity.status(204).build();
            }
            return ResponseEntity.status(200).body(studentList);
        } catch (Exception e) {
            return ResponseEntity.status(500).body(null);
        }
    }
}
