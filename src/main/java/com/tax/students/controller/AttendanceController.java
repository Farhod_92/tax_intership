package com.tax.students.controller;

import com.tax.students.entity.Attendance;
import com.tax.students.payload.ApiResponse;
import com.tax.students.service.AttendanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Validated
@RestController
@RequestMapping("/attendance")
@Api(tags = "AttendanceController", description = "Studentlar davomadi")
public class AttendanceController {
    @Autowired
    private  AttendanceService attendanceService;

    @ApiOperation(value = "Id bilan davomatni(attendance) olish", notes = "Attendance - student o'qishga kelgan yoki haqidagi ma'lumot")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Success"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getById(@PathVariable @Min(value = 0, message = "required positive id") Integer id){
        ApiResponse apiResponse = attendanceService.getById(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "Barcha davomatlarni olish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Success"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found ")
    })
    @GetMapping("/all")
    public ResponseEntity< List<Attendance> > getAll(){
        List<Attendance>  attendanceList = attendanceService.getAll();
        return ResponseEntity.ok(attendanceList);
    }

    @ApiOperation(value = "Id bilan davomatni(attendance) o'chirish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "Successfully deleted"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "error")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> delete(@PathVariable @Min(value = 0, message = "required positive id") Integer id){
        ApiResponse apiResponse = attendanceService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    @ApiOperation(value = "Kunlik studenlarning o'qishga kechikkan va vaqtida kelgani bo'icha guruhlangan holda excel fayliga yuklab olish", notes = "so'rovdagi kun formati: yyy-MM-dd")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfully retrieved"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found "),
            @io.swagger.annotations.ApiResponse(code = 409, message = "error")
    })
    @GetMapping("/report")
    public void getReport(@Pattern(regexp = "^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$", message = "date format is yyy-MM-dd ") @RequestParam String date, HttpServletResponse response){
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=attendance_report_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
        attendanceService.getReport(date, response);
    }

}
