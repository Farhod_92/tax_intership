package com.tax.students.controller;

import com.tax.students.entity.Student;
import com.tax.students.payload.ApiResponse;
import com.tax.students.payload.AttendanceDTO;
import com.tax.students.payload.StudentDTO;
import com.tax.students.service.PersonalDetailsService;
import com.tax.students.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.*;

@Validated
@RestController
@RequestMapping("/student")
@Api(tags = "StudentController", description = "Studentlar ma'lumotlarini boshqarish")
public class StudentController {
    @Autowired
    private  StudentService studentService;
    @Autowired
    private  PersonalDetailsService personalDetailsService;

    @ApiOperation(value = "Student qo'shish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "Successfully added"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @PostMapping
    public ResponseEntity<ApiResponse> addStudent(@Valid @RequestBody StudentDTO studentDTO){
        ApiResponse apiResponse = studentService.addStudent(studentDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @ApiOperation(value = "Id bilan Studentni olish", notes = "Apiresponseni obektida student obekti qaytadi")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getStudentById(@PathVariable @Min(value = 0, message = "required positive id") Integer id){
        ApiResponse apiResponse = studentService.getStudentById(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "Barcha Studentlarni olish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found")
    })
    @GetMapping("/all")
    public ResponseEntity<List<Student>> getAllStudents(){
        List<Student>  studentList = studentService.getAllStudents();
        return ResponseEntity.ok(studentList);
    }

    @ApiOperation(value = "Studentni ma'lumotlarini o'zgartirish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editStudent(@PathVariable Integer id,@Valid @RequestBody StudentDTO studentDTO){
        ApiResponse apiResponse = studentService.editStudent(id, studentDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "Student o'qishga kelgan yoki ketganligi to'grisida ma'lumot qo'shish", notes = "come=true bo'lsa kelgan, false bo'lsa ketgan bo'ladi")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 200, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @PostMapping("/comeOrGo")
    public ResponseEntity<ApiResponse> studentComeOrGo(@Valid @RequestBody AttendanceDTO attendanceDTO){
        ApiResponse apiResponse = studentService.studentComeOrGo(attendanceDTO);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @ApiOperation(value = "Id bilan Studentni o'chirish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 204, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<ApiResponse> deleteStudent(@PathVariable @Min(value = 0, message = "required positive id") Integer id){
        ApiResponse apiResponse = studentService.deleteStudent(id);
        return ResponseEntity.status(apiResponse.isSuccess()?204:409).body(apiResponse);
    }

    @ApiOperation(value = "Studentga rasm qoshish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 201, message = "Successfull"),
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found"),
            @io.swagger.annotations.ApiResponse(code = 409, message = "conflict")
    })
    @PostMapping("/addPhoto/{studentId}")
    public ResponseEntity<ApiResponse> addPhotoToStudent(@RequestParam MultipartFile file, @PathVariable @Min(value = 0, message = "required positive id") Integer studentId){
       ApiResponse apiResponse = studentService.addPhotoToStudent(file, studentId);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @ApiOperation(value = "Studentning resumesini pdf fayl ko'rinishida yuklab olish")
    @ApiResponses(value = {
            @io.swagger.annotations.ApiResponse(code = 404, message = "Not found")

    })
    @GetMapping("/details/{studentId}")
    public void download(@PathVariable @Min(value = 0, message = "required positive id") Integer studentId, HttpServletResponse response){
        personalDetailsService.getPersonalDetails(studentId, response);
    }

}
