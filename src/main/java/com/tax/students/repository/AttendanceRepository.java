package com.tax.students.repository;

import com.tax.students.entity.Attendance;
import com.tax.students.payload.AttendanceReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AttendanceRepository extends JpaRepository<Attendance, Integer> {
    @Query(value = "select s.gr, s.name, s.sure_name, s.age, s.course, s.sex, s.phone_number as phone, s.email, to_char(a.time, 'dd.mm.yyyy HH24:MI:SS') as time from attendance a join  excel_student s on  a.student_id=s.id  where cast(time\\:\\:date as varchar ) = :dateString and time\\:\\:time>'09:00' and come=true", nativeQuery = true)
    List<AttendanceReport> getLateStudents(String dateString);

    @Query(value = "select s.gr, s.name, s.sure_name, s.age, s.course, s.sex, s.phone_number as phone, s.email, to_char(a.time, 'dd.mm.yyyy HH24:MI:SS') as time from attendance a join excel_student s on  a.student_id=s.id  where cast(time\\:\\:date as varchar ) = :dateString and time\\:\\:time<='09:00' and come=true", nativeQuery = true)
    List<AttendanceReport> getEarlyArrivedStudents(String dateString);
}
