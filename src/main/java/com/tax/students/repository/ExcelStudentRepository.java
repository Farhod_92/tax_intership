package com.tax.students.repository;

import com.tax.students.entity.ExcelStudent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExcelStudentRepository extends JpaRepository<ExcelStudent, Integer> {
}
