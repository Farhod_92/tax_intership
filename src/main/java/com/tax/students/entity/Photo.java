package com.tax.students.entity;

import com.tax.students.entity.template.AbsEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Photo extends AbsEntity {

    private String fileOriginalName; //test.txt, test.png

    private long size;

    private String contentType;

    private String name; //papkani ichidagi nomi

    private Integer studentId;
}
