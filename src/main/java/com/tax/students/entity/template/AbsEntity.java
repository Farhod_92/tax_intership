package com.tax.students.entity.template;

import lombok.Data;

import javax.persistence.*;

@MappedSuperclass
@Data
public abstract class AbsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Integer id;
}
