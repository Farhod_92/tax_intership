package com.tax.students.entity;

import com.tax.students.entity.template.AbsEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;


@ApiModel(value = "")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Attendance extends AbsEntity {
    @ManyToOne(optional = false)
    private Student student;

    //true=come, false=went
    @Column(nullable = false)
    private boolean come;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp time;
}
