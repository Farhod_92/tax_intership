package com.tax.students.entity;

import com.tax.students.entity.template.AbsEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.Entity;

@ApiModel(description = "Excel faylidagi Student ma'lumotlari")
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ExcelStudent extends AbsEntity {
    @ApiModelProperty(value = "guruhi")
    private String gr;
    @ApiModelProperty(value = "ismi")
    private String name;
    @ApiModelProperty(value = "familiyasi")
    private String sureName;
    @ApiModelProperty(value = "yoshi")
    private Integer age;
    @ApiModelProperty(value = "o'qiydigan kursi")
    private String course;
    @ApiModelProperty(value = "jinsi")
    private String sex;
    @ApiModelProperty(value = "telefon raqami")
    private String phoneNumber;
    @ApiModelProperty(value = "email adresi")
    private String email;

}
