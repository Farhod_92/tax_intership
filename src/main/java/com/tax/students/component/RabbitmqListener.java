package com.tax.students.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tax.students.payload.AttendanceDTO;
import com.tax.students.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@EnableRabbit
@Component
public class RabbitmqListener {
    Logger logger = LoggerFactory.getLogger(RabbitmqListener.class);

    @Autowired
    private StudentService studentService;

    @RabbitListener(queues = "attendanceQueue")
    public void processMyQueue(String message){
        ObjectMapper objectMapper = new ObjectMapper();
        AttendanceDTO attendanceDTO = null;
        try {
            attendanceDTO = objectMapper.readValue(message, AttendanceDTO.class);
            studentService.studentComeOrGo(attendanceDTO);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        logger.info("Received from attendanceQueue: " + attendanceDTO.getStudentId());
    }

}
