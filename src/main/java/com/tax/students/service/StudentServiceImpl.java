package com.tax.students.service;

import com.tax.students.entity.Attendance;
import com.tax.students.entity.Photo;
import com.tax.students.entity.Student;
import com.tax.students.payload.ApiResponse;
import com.tax.students.payload.AttendanceDTO;
import com.tax.students.payload.StudentDTO;
import com.tax.students.repository.AttendanceRepository;
import com.tax.students.repository.ExcelStudentRepository;
import com.tax.students.repository.PhotoRepository;
import com.tax.students.repository.StudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final AttendanceRepository attendanceRepository;
    private final PhotoRepository photoRepository;
    private final ExcelStudentRepository excelStudentRepository;

    private StudentServiceImpl(StudentRepository studentRepository, AttendanceRepository attendanceRepository, PhotoRepository photoRepository, ExcelStudentRepository excelStudentRepository) {
        this.studentRepository = studentRepository;
        this.attendanceRepository = attendanceRepository;
        this.photoRepository = photoRepository;
        this.excelStudentRepository = excelStudentRepository;
    }

    @Override
    public ApiResponse addStudent(StudentDTO studentDTO) {
        try {
            Student student = new Student(studentDTO.getFullName(), studentDTO.getAge());
            studentRepository.save(student);
            return new ApiResponse("added new student", true);
        }catch (Exception e) {
            return new ApiResponse("error", false);
        }

    }

    @Override
    public ApiResponse studentComeOrGo(AttendanceDTO attendanceDTO) {
        Optional<Student> optionalStudent = studentRepository.findById(attendanceDTO.getStudentId());
        if(!optionalStudent.isPresent())
            return new ApiResponse("student by this id not found", false);

        Student student = optionalStudent.get();
        Attendance attendance = new Attendance();
        attendance.setStudent(student);
        attendance.setCome(attendanceDTO.isCome());
        attendanceRepository.save(attendance);
        return new ApiResponse("student came or went", true);
    }

    @Override
    public ApiResponse getStudentById(Integer id) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if(!optionalStudent.isPresent())
            return new ApiResponse("student not found", false);
        return new ApiResponse("student by id", true, optionalStudent.get());
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public ApiResponse editStudent(Integer id, StudentDTO studentDTO) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if(!optionalStudent.isPresent())
            return new ApiResponse("student not found", false);
        Student student = optionalStudent.get();
        student.setFullName(studentDTO.getFullName());
        student.setAge(studentDTO.getAge());
        studentRepository.save(student);
        return new ApiResponse("student updated", true);
    }

    @Override
    public ApiResponse deleteStudent(Integer id) {
        try {
            studentRepository.deleteById(id);
            return new ApiResponse("student deleted", true);
        }catch (Exception e){
            return new ApiResponse("error", false);
        }
    }

    @Override
    public ApiResponse addPhotoToStudent(MultipartFile file, Integer studentId) {
        boolean existsById = excelStudentRepository.existsById(studentId);
        if(!existsById)
            return new ApiResponse("student not found", false);

        if(file != null) {
            try{
                String originalFilename = file.getOriginalFilename();
                long size = file.getSize();
                String contentType = file.getContentType();

                Photo photo = new Photo();
                photo.setFileOriginalName(originalFilename);
                photo.setSize(size);
                photo.setContentType(contentType);
                String[] split = originalFilename.split("\\.");
                String name = UUID.randomUUID().toString() + "." + split[split.length - 1];
                photo.setName(name);
                photo.setStudentId(studentId);
                photoRepository.save(photo);

                Path path = Paths.get("src/main/resources/photo/" + name);
                Files.copy(file.getInputStream(), path);
                return new ApiResponse("added photo to student", true);
            } catch (Exception e) {
                e.printStackTrace();
                return new ApiResponse("error add photo", false);
            }
        }
        return new ApiResponse("empty file", false);
    }
}
