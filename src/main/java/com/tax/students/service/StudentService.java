package com.tax.students.service;

import com.tax.students.entity.Student;
import com.tax.students.payload.ApiResponse;
import com.tax.students.payload.AttendanceDTO;
import com.tax.students.payload.StudentDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface StudentService {
    ApiResponse addStudent(StudentDTO studentDTO);

    ApiResponse studentComeOrGo(AttendanceDTO attendanceDTO);

    ApiResponse getStudentById(Integer id);

    List<Student> getAllStudents();

    ApiResponse editStudent(Integer id, StudentDTO studentDTO);

    ApiResponse deleteStudent(Integer id);

    ApiResponse addPhotoToStudent(MultipartFile file, Integer studentId);

}
