package com.tax.students.service;

import com.tax.students.payload.AttendanceReport;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

public class ExporterToExcelTemplate {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<AttendanceReport> lateStudents;
    private List<AttendanceReport> earlyArrivedStudents;
    private int rowCount = 2;

    private CellStyle style;
    private XSSFFont font;
    File file;


    public ExporterToExcelTemplate(List<AttendanceReport> lateStudents, List<AttendanceReport> earlyArrivedStudents) {
        this.lateStudents = lateStudents;
        this.earlyArrivedStudents = earlyArrivedStudents;

        file = new File("src/main/resources/excel/Students.xlsx");
    }

    public void export(HttpServletResponse response, String date) throws IOException {
        File newFile = new File("src/main/resources/excel/"+ UUID.randomUUID()+".xlsx");
        newFile.createNewFile();
        Files.copy(file.toPath(),newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        try {
            workbook = new XSSFWorkbook(newFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        style = workbook.createCellStyle();
        font = workbook.createFont();
        style = workbook.createCellStyle();
        font = workbook.createFont();
        style.setFont(font);
        font.setFontHeight(12);
        sheet = workbook.getSheetAt(0);

        writeDataLines(earlyArrivedStudents, date+ " kuni vaqtli kelgan studentlar");
        writeDataLines(lateStudents, date + " kuni kech kelgan studentlar");

        workbook.write(response.getOutputStream());
        workbook.close();

        newFile.delete();
    }

    private void writeDataLines(List<AttendanceReport> list, String title) {

        Row row1 = sheet.createRow(rowCount++);
        createCell(row1, 1, title, style);

        for (AttendanceReport report : list) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
            createCell(row, columnCount++, report.getName(), style);
            createCell(row, columnCount++, report.getSureName(), style);
            createCell(row, columnCount++, report.getAge(), style);
            createCell(row, columnCount++, report.getCourse(), style);
            createCell(row, columnCount++, report.getEmail(), style);
            createCell(row, columnCount++, report.getGr(), style);
            createCell(row, columnCount++, report.getSex(), style);
            createCell(row, columnCount++, report.getPhone(), style);
            createCell(row, columnCount++, report.getTime(), style);
        }
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

}
