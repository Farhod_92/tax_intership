package com.tax.students.service;

import com.tax.students.entity.ExcelStudent;
import com.tax.students.repository.ExcelStudentRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ExcelServiceImpl implements ExcelService {
    private final ExcelStudentRepository excelStudentRepository;

    public ExcelServiceImpl(ExcelStudentRepository excelStudentRepository) {
        this.excelStudentRepository = excelStudentRepository;
    }

    public void save(MultipartFile file) {
        try {
            List<ExcelStudent> studentList = ExcelToDbImporter.excelToStudents(file.getInputStream());
            excelStudentRepository.saveAll(studentList);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
    public List<ExcelStudent> getAllExcelStudents() {
        return excelStudentRepository.findAll();
    }
}
