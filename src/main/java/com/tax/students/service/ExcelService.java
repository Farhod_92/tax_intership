package com.tax.students.service;

import com.tax.students.entity.ExcelStudent;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ExcelService {
    void save(MultipartFile file);

    List<ExcelStudent> getAllExcelStudents();
}
