package com.tax.students.service;

import com.tax.students.entity.ExcelStudent;
import com.tax.students.entity.Photo;
import com.tax.students.repository.ExcelStudentRepository;
import com.tax.students.repository.PhotoRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;


@Service
public class PersonalDetailsService {
    private final PhotoRepository photoRepository;
    private final ExcelStudentRepository excelStudentRepository;

    private String photoUrl;


    public PersonalDetailsService(PhotoRepository photoRepository, ExcelStudentRepository excelStudentRepository) {
        this.photoRepository = photoRepository;
        this.excelStudentRepository = excelStudentRepository;
    }

    public  void getPersonalDetails(Integer studentID, HttpServletResponse response){
        Optional<ExcelStudent> studentOptional = excelStudentRepository.findById(studentID);
        if(!studentOptional.isPresent())
            return;

        ExcelStudent excelStudent = studentOptional.get();

        Optional<Photo> optionalPhoto = photoRepository.findByStudentId(studentID);
        if(optionalPhoto.isPresent())
            photoUrl = "src/main/resources/photo/"+optionalPhoto.get().getName();
        else
            photoUrl = "src/main/resources/photo/noPhoto.png";

        PdfSender pdfSender = new PdfSender();
        pdfSender.getPersonalDetails(response, photoUrl, excelStudent);



    }
}