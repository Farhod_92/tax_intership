package com.tax.students.service;

import com.tax.students.entity.ExcelStudent;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import org.apache.poi.ss.usermodel.Row;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelToDbImporter {
    public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static boolean hasExcelFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }
    public static List<ExcelStudent> excelToStudents(InputStream is) {
        try {
            Workbook workbook = new XSSFWorkbook(is);
            Sheet sheet = workbook.getSheet(workbook.getSheetName(0));
            Iterator<Row> rows = sheet.iterator();
            List<ExcelStudent> tutorials = new ArrayList<>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();
                // skip header
                if (rowNumber == 0 || rowNumber == 1) {
                    rowNumber++;
                    continue;
                }
                Iterator<Cell> cellsInRow = currentRow.iterator();
                ExcelStudent student = new ExcelStudent();
                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();
                    switch (cellIdx) {
                        case 0:
                            student.setGr(currentCell.getStringCellValue());
                            break;
                        case 1:
                            student.setName(currentCell.getStringCellValue());
                            break;
                        case 2:
                            student.setSureName(currentCell.getStringCellValue());
                            break;
                        case 3:
                            student.setAge((int)currentCell.getNumericCellValue());
                            break;
                        case 4:
                            student.setCourse(currentCell.getStringCellValue());
                            break;
                        case 5:
                            student.setSex(currentCell.getStringCellValue());
                            break;
                        case 6:
                            student.setPhoneNumber(currentCell.getStringCellValue());
                            break;
                        case 7:
                            student.setEmail(currentCell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    cellIdx++;
                }
                tutorials.add(student);
            }
            workbook.close();
            return tutorials;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

}
