package com.tax.students.service;

import com.tax.students.entity.Attendance;
import com.tax.students.payload.ApiResponse;
import com.tax.students.payload.AttendanceReport;
import com.tax.students.repository.AttendanceRepository;
import com.tax.students.repository.ExcelStudentRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class AttendanceServiceImpl implements AttendanceService{
    private final AttendanceRepository attendanceRepository;
    private final ExcelStudentRepository excelStudentRepository;

    private AttendanceServiceImpl(AttendanceRepository attendanceRepository, ExcelStudentRepository excelStudentRepository) {
        this.attendanceRepository = attendanceRepository;
        this.excelStudentRepository = excelStudentRepository;
    }

    @Override
    public ApiResponse delete(Integer id) {
        try {
            attendanceRepository.deleteById(id);
            return new ApiResponse("deleted", true);
        }catch (Exception e){
            return new ApiResponse("error", false);
        }
    }

    @Override
    public List<Attendance> getAll() {
        return attendanceRepository.findAll();
    }

    @Override
    public ApiResponse getById(Integer id) {
        Optional<Attendance> optionalAttendance = attendanceRepository.findById(id);
        if(!optionalAttendance.isPresent())
            return new ApiResponse("Attendance not found", false);
        return new ApiResponse("Attendance by id", true, optionalAttendance.get());
    }

    @Override
    public void getReport(String date, HttpServletResponse response) {
        List<AttendanceReport> late = attendanceRepository.getLateStudents(date);

        List<AttendanceReport> early = attendanceRepository.getEarlyArrivedStudents(date);

//        ExporterToExcel exporterToExcel = new ExporterToExcel(late, early);
        ExporterToExcelTemplate exporterToExcel = new ExporterToExcelTemplate(late, early);
        try {
            exporterToExcel.export(response, date);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
