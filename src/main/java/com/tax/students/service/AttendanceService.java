package com.tax.students.service;

import com.tax.students.entity.Attendance;
import com.tax.students.payload.ApiResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface AttendanceService {
    ApiResponse delete(Integer id);

    List<Attendance> getAll();

    ApiResponse getById(Integer id);

    void getReport(String date, HttpServletResponse response);
}
