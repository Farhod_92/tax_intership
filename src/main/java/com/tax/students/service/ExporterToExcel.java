package com.tax.students.service;

import com.tax.students.payload.AttendanceReport;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ExporterToExcel {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<AttendanceReport> lateStudents;
    private List<AttendanceReport> earlyArrivedStudents;
    int rowCount = 1;

    CellStyle style;
    XSSFFont font;


    public ExporterToExcel(List<AttendanceReport> lateStudents, List<AttendanceReport> earlyArrivedStudents) {
        this.lateStudents = lateStudents;
        this.earlyArrivedStudents = earlyArrivedStudents;
        workbook = new XSSFWorkbook();
        style = workbook.createCellStyle();
        font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);
    }

    public void export(HttpServletResponse response, String date) throws IOException {
        writeHeaderLine();
        writeDataLines(earlyArrivedStudents, date+ " kuni vaqtli kelgan studentlar");
        writeDataLines(lateStudents, date + " kunikech kelgan studentlar");

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Attendance");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCell(row, 0, "name", style);
        createCell(row, 1, "surename", style);
        createCell(row, 2, "age", style);
        createCell(row, 3, "course", style);
        createCell(row, 4, "email", style);
        createCell(row, 5, "group", style);
        createCell(row, 6, "sex", style);
        createCell(row, 7, "phone number", style);
        createCell(row, 8, "time", style);

    }

    private void writeDataLines(List<AttendanceReport> list, String title) {
        Row row1 = sheet.createRow(rowCount++);
        createCell(row1, 1, title, style);

        for (AttendanceReport report : list) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
            createCell(row, columnCount++, report.getName(), style);
            createCell(row, columnCount++, report.getSureName(), style);
            createCell(row, columnCount++, report.getAge(), style);
            createCell(row, columnCount++, report.getCourse(), style);
            createCell(row, columnCount++, report.getEmail(), style);
            createCell(row, columnCount++, report.getGr(), style);
            createCell(row, columnCount++, report.getSex(), style);
            createCell(row, columnCount++, report.getPhone(), style);
            createCell(row, columnCount++, report.getTime(), style);
        }
    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

}
