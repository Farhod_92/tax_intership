package com.tax.students.service;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tax.students.entity.ExcelStudent;
import com.tax.students.payload.WorkHistory;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class PdfSender {
    private final String FONT = "c:/windows/fonts/Arial.ttf";

    private static BaseFont baseFont;
    private static Font font;
    private static Font boldFont;
    private PdfPTable table = new PdfPTable(3);

    public  void getPersonalDetails(HttpServletResponse response, String photoUrl, ExcelStudent excelStudent){

        List<WorkHistory> workHistoryList = new ArrayList<>(
                Arrays.asList(
                        new WorkHistory("TATU", "2014-2017"),
                        new WorkHistory("MV", "2017-2018"),
                        new WorkHistory("RTATM", "2018-2020"),
                        new WorkHistory("MV", "2020-2022")
                )
        );

        Document document = new Document(PageSize.A4, 50, 50, 20, 50);

        //AGAR BU SHRIFT TOPILMASA
        File shriftFile = new File(FONT);
        if(!shriftFile.exists()){
        }

        try {
            baseFont = BaseFont.createFont(FONT, BaseFont.IDENTITY_H, true);
            font = new Font(baseFont, 14);
            boldFont = new Font(baseFont, 14, Font.BOLD);

            File file = new File("src/main/resources/pdfFiles/"+ UUID.randomUUID()+"_document.pdf");
            file.createNewFile();

            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            Paragraph title = new Paragraph("RESUME", boldFont);
            title.setAlignment(Element.ALIGN_CENTER);
            document.add(title);

            ImageData data = ImageDataFactory.create(photoUrl);
            Image image = Image.getInstance(data.getUrl());
            image.scaleToFit(80,80);
            image.setAlignment(Element.ALIGN_RIGHT);
            document.add(image);

            document.add(new Paragraph("Name: "+excelStudent.getName() + "\n", font));
            document.add(new Paragraph("Surname: "+excelStudent.getSureName() + "\n", font));
            document.add(new Paragraph("Age: "+excelStudent.getAge() + "\n", font));
            document.add(new Paragraph("Email: "+excelStudent.getEmail() + "\n", font));
            document.add(new Paragraph("Telefon: "+excelStudent.getPhoneNumber() + "\n", font));

            Paragraph tableTitle = new Paragraph("Jadval", boldFont);
            tableTitle.setAlignment(Element.ALIGN_CENTER);
            document.add(tableTitle);
            document.add(new Paragraph("\n"));

            table.setTotalWidth(new float[]{50, 100, 380});
            table.setLockedWidth(true);


            createCell("№", Element.ALIGN_CENTER, boldFont);
            createCell("Davr ", Element.ALIGN_CENTER, boldFont);
            createCell("Ish joyi ", Element.ALIGN_CENTER, boldFont);
            for (int i = 0; i < workHistoryList.size(); i++) {
                createCell(String.valueOf(i+1), Element.ALIGN_CENTER, font );
                createCell(workHistoryList.get(i).getDavr(), Element.ALIGN_LEFT, font);
                createCell(workHistoryList.get(i).getJoy(), Element.ALIGN_LEFT, font);
            }

            document.add(table);
            document.close();

            FileInputStream inputStream = new FileInputStream(file);
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            file.delete();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createCell(String text, int horizontalAlignment, Font font){
        PdfPCell cell = new PdfPCell(new Phrase(text, font));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(horizontalAlignment);
        cell.setPadding(5);
        table.addCell(cell);
    }
//
//    private void createImageCell(Image image){
//        PdfPCell pdfPCell = new PdfPCell(image);
//        pdfPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//        pdfPCell.setPadding(2);
//        table.addCell(pdfPCell);
//    }
}
