package com.tax.students.payload;

public interface AttendanceReport {
     String getGr();
     String getName();
     String getSureName();
     Integer getAge();
     String getCourse();
     String getSex();
     String getPhone();
     String getEmail();
     String getTime();
}
