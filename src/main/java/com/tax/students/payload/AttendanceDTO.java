package com.tax.students.payload;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@ApiModel(description = "studentlarning kelishi yoki ketishini saqlash uchun model ")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttendanceDTO {
    @ApiModelProperty(value = "student idsi")
    @NotNull(message = "student id can't be empty")
    private Integer studentId;

    @ApiModelProperty(value = "kelgan bo'lsa true, ketgan bo'lsa false")
    @NotNull(message = "student must come or go")
    private boolean come;
}
