package com.tax.students.payload;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description = "Barcha so'rovlarga javob sifatida qaytuvchi obekt")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    @ApiModelProperty(value = "response holati haqida ma'lumot")
    private String message;
    @ApiModelProperty(value = "response muvaffaqiyatlimi yoqmi")
    private boolean success;
    @ApiModelProperty(value = "agar obekti bor bo'lsa responsning obekti")
    private Object object;

    public ApiResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }
}
