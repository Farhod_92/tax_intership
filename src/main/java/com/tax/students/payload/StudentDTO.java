package com.tax.students.payload;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@ApiModel(description = "Student qo'shish uchun  model")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {
    @ApiModelProperty(value = "to'liq ismi")
    @NotEmpty(message = "full name can't be empty")
    private String fullName;

    @ApiModelProperty(value = "yoshi")
    @NotNull(message = "positive number value is required")
    @Min(value=15, message="positive number, min 15 is required")
    private Integer age;
}
